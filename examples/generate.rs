use nbt::NBT;
use std::{collections::HashMap, fs::File};

fn main() {
    NBT::from(HashMap::from([("a".try_into().unwrap(), 69_i16.into())]))
        .serialise(&mut File::create("a.nbt").unwrap())
        .unwrap();
    NBT::from(HashMap::from([("b".try_into().unwrap(), 420_i16.into())]))
        .serialise(&mut File::create("b.nbt").unwrap())
        .unwrap();
}
