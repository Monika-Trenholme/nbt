use nbt::NBT;
use std::{env::args, fs::File, io::BufReader};

fn main() {
    let mut args = args();
    args.next();
    let input = args.next().unwrap();
    let file = File::open(input).unwrap();
    let nbt = NBT::deserialise(&mut BufReader::new(file)).unwrap();
    println!("{nbt}");
}
