use nbt::NBT;
use std::{
    env::args,
    fs::File,
    io::{BufReader, BufWriter},
};

fn main() {
    let mut args = args();
    args.next();
    let input = args.next().unwrap();
    let output = args.next().unwrap();
    let file = File::open(input).unwrap();
    let nbt = NBT::deserialise(&mut BufReader::new(file)).unwrap();
    let file = File::create(output).unwrap();
    nbt.serialise(&mut BufWriter::new(file)).unwrap();
}
