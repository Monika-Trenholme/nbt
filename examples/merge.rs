use nbt::NBT;
use std::{collections::HashMap, env::args, fs::File};

fn main() {
    let mut args = args();
    args.next();
    let a = args.next().unwrap();
    let b = args.next().unwrap();
    let mut a: HashMap<_, _> = NBT::deserialise(&mut File::open(a).unwrap())
        .unwrap()
        .try_into()
        .unwrap();
    let b: HashMap<_, _> = NBT::deserialise(&mut File::open(b).unwrap())
        .unwrap()
        .try_into()
        .unwrap();
    a.extend(b.into_iter());
    println!("{}", NBT::from(a));
}
