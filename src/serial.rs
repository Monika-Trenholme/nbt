use super::nbt::*;
use std::{
    collections::HashMap,
    error, fmt,
    io::{self, Read, Write},
    mem::{size_of, transmute},
    string::FromUtf8Error,
};

// Depth limit for binary format
const DEPTH_LIMIT: u16 = 512;

#[derive(Debug)]
pub enum Error {
    UnexpectedEOF,
    InvalidTagId(u8),
    DuplicateEntry(String),
    InvalidListTypeTag(u8),
    Depth,
    NotCompound,
    IO(io::Error),
    UTF8(FromUtf8Error),
}

impl From<io::Error> for Error {
    fn from(e: io::Error) -> Self {
        Self::IO(e)
    }
}

impl From<FromUtf8Error> for Error {
    fn from(e: FromUtf8Error) -> Self {
        Self::UTF8(e)
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::UnexpectedEOF => write!(f, "unexpected end of file"),
            Self::InvalidTagId(tag) => write!(f, "invalid tag id {tag}"),
            Self::DuplicateEntry(name) => write!(f, "duplicate entry {name} found in compound"),
            Self::InvalidListTypeTag(tag) => write!(
                f,
                "list has invalid type tag id {tag} despite having a length greater than 0"
            ),
            Self::Depth => write!(
                f,
                "Max depth for compunds and lists of {DEPTH_LIMIT} exceeded"
            ),
            Self::NotCompound => write!(f, "Attempt to serialise non compound value"),
            Self::IO(e) => write!(f, "{e}"),
            Self::UTF8(e) => write!(f, "{e}"),
        }
    }
}

impl error::Error for Error {}

pub type Result<T> = std::result::Result<T, Error>;

fn read_num<T: Copy>(reader: &mut dyn Read) -> Result<T> {
    let mut buffer = [0; 8];
    let buffer = &mut buffer[..size_of::<T>()];
    if reader.read(buffer)? == size_of::<T>() {
        #[cfg(target_endian = "little")]
        buffer.reverse();
        Ok(unsafe { *transmute::<*const u8, *const T>(buffer.as_ptr()) })
    } else {
        Err(Error::UnexpectedEOF)
    }
}

fn read_tag(reader: &mut dyn Read) -> Result<u8> {
    let tag = read_num::<u8>(reader)?;
    if tag > 12 {
        return Err(Error::InvalidTagId(tag));
    }
    Ok(tag)
}

fn read_string(reader: &mut dyn Read) -> Result<NBTString> {
    let len = read_num::<u16>(reader)? as usize;
    let mut buffer = vec![0; len];
    if reader.read(&mut buffer)? == len {
        Ok(NBTString(String::from_utf8(buffer)?))
    } else {
        Err(Error::UnexpectedEOF)
    }
}

fn read_num_array<T: Copy>(reader: &mut dyn Read) -> Result<Vec<T>> {
    let len = read_num::<i32>(reader)? as usize;
    std::iter::repeat_with(|| read_num::<T>(reader))
        .take(len)
        .collect()
}

fn read_list_payload(reader: &mut dyn Read, depth: u16) -> Result<List> {
    if depth > DEPTH_LIMIT {
        return Err(Error::Depth);
    }
    let tag = read_tag(reader)?;
    let len = read_num::<i32>(reader)?;
    if len <= 0 {
        return Ok(List(tag, Vec::new()));
    }
    if tag == TAG_END {
        return Err(Error::InvalidListTypeTag(tag));
    }
    let mut items = Vec::new();
    for _ in 0..len {
        items.push(read_payload(reader, tag, depth + 1)?);
    }
    Ok(List(tag, items))
}

pub(crate) fn read_compound_payload(
    reader: &mut dyn Read,
    depth: u16,
) -> Result<HashMap<NBTString, NBT>> {
    if depth > DEPTH_LIMIT {
        return Err(Error::Depth);
    }
    let mut map = HashMap::new();
    loop {
        let tag = read_tag(reader);
        let tag = match tag {
            Ok(tag) => tag,
            Err(Error::UnexpectedEOF) if depth == 0 => 0,
            Err(e) => return Err(e),
        };
        if tag == TAG_END {
            break;
        }
        let name = read_string(reader)?;
        if map.contains_key(&name) {
            return Err(Error::DuplicateEntry(name.into()));
        } else {
            map.insert(name, read_payload(reader, tag, depth + 1)?);
        }
    }
    Ok(map)
}

fn read_payload(reader: &mut dyn Read, tag: u8, depth: u16) -> Result<NBT> {
    match tag {
        TAG_BYTE => Ok(read_num::<i8>(reader)?.into()),
        TAG_SHORT => Ok(read_num::<i16>(reader)?.into()),
        TAG_INT => Ok(read_num::<i32>(reader)?.into()),
        TAG_LONG => Ok(read_num::<i64>(reader)?.into()),
        TAG_FLOAT => Ok(read_num::<f32>(reader)?.into()),
        TAG_DOUBLE => Ok(read_num::<f64>(reader)?.into()),
        TAG_BYTE_ARRAY => Ok(NBT(NBTInner::ByteArray(read_num_array::<i8>(reader)?))),
        TAG_STRING => Ok(NBT(NBTInner::String(read_string(reader)?))),
        TAG_LIST => Ok(read_list_payload(reader, depth)?.into()),
        TAG_COMPOUND => Ok(read_compound_payload(reader, depth)?.into()),
        TAG_INT_ARRAY => Ok(NBT(NBTInner::IntArray(read_num_array::<i32>(reader)?))),
        TAG_LONG_ARRAY => Ok(NBT(NBTInner::LongArray(read_num_array::<i64>(reader)?))),
        _ => unreachable!(),
    }
}

fn write_num<T: 'static>(writer: &mut dyn Write, mut num: T) -> Result<()> {
    let bytes = unsafe {
        std::slice::from_raw_parts_mut(
            transmute::<&mut T, &mut u8>(&mut num) as *mut u8,
            size_of::<T>(),
        )
    };
    #[cfg(target_endian = "little")]
    bytes.reverse();
    writer.write_all(bytes)?;
    Ok(())
}

fn write_str(writer: &mut dyn Write, str: &NBTString) -> Result<()> {
    let bytes = str.0.as_bytes();
    write_num(writer, bytes.len() as u16)?;
    writer.write_all(bytes)?;
    Ok(())
}

fn write_num_array<T: 'static + Copy>(writer: &mut dyn Write, nums: &[T]) -> Result<()> {
    write_num(writer, nums.len() as i32)?;
    for num in nums {
        write_num(writer, *num)?;
    }
    Ok(())
}

fn write_list_payload(writer: &mut dyn Write, list: &List, depth: u16) -> Result<()> {
    if depth > DEPTH_LIMIT {
        return Err(Error::Depth);
    }
    let (tag, vec) = list.parts();
    write_num(writer, tag)?;
    write_num(writer, vec.len() as i32)?;
    for item in vec {
        write_payload(writer, item, depth + 1)?;
    }
    Ok(())
}

pub(crate) fn write_compound_payload(
    writer: &mut dyn Write,
    map: &HashMap<NBTString, NBT>,
    depth: u16,
) -> Result<()> {
    if depth > DEPTH_LIMIT {
        return Err(Error::Depth);
    }
    for (name, item) in map {
        write_num(writer, item.tag())?;
        write_str(writer, name)?;
        write_payload(writer, item, depth + 1)?;
    }
    if depth != 0 {
        write_num(writer, TAG_END)?;
    }
    Ok(())
}

fn write_payload(writer: &mut dyn Write, nbt: &NBT, depth: u16) -> Result<()> {
    match nbt {
        &NBT(NBTInner::Byte(byte)) => write_num(writer, byte),
        &NBT(NBTInner::Short(short)) => write_num(writer, short),
        &NBT(NBTInner::Int(int)) => write_num(writer, int),
        &NBT(NBTInner::Long(long)) => write_num(writer, long),
        &NBT(NBTInner::Float(float)) => write_num(writer, float),
        &NBT(NBTInner::Double(double)) => write_num(writer, double),
        NBT(NBTInner::ByteArray(bytes)) => write_num_array(writer, bytes),
        NBT(NBTInner::String(string)) => write_str(writer, string),
        NBT(NBTInner::List(list)) => write_list_payload(writer, list, depth),
        NBT(NBTInner::Compound(map)) => write_compound_payload(writer, map, depth),
        NBT(NBTInner::IntArray(ints)) => write_num_array(writer, ints),
        NBT(NBTInner::LongArray(longs)) => write_num_array(writer, longs),
    }
}
