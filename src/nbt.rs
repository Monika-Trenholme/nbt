use super::serial::{self, read_compound_payload, write_compound_payload};
use std::{
    collections::HashMap,
    fmt,
    io::{Read, Write},
};

pub const TAG_END: u8 = 0;
pub const TAG_BYTE: u8 = 1;
pub const TAG_SHORT: u8 = 2;
pub const TAG_INT: u8 = 3;
pub const TAG_LONG: u8 = 4;
pub const TAG_FLOAT: u8 = 5;
pub const TAG_DOUBLE: u8 = 6;
pub const TAG_BYTE_ARRAY: u8 = 7;
pub const TAG_STRING: u8 = 8;
pub const TAG_LIST: u8 = 9;
pub const TAG_COMPOUND: u8 = 10;
pub const TAG_INT_ARRAY: u8 = 11;
pub const TAG_LONG_ARRAY: u8 = 12;

#[derive(PartialEq, Eq, Hash)]
pub struct NBTString(pub(crate) String);

pub struct List(pub(crate) u8, pub(crate) Vec<NBT>);

pub(crate) enum NBTInner {
    Byte(i8),
    Short(i16),
    Int(i32),
    Long(i64),
    Float(f32),
    Double(f64),
    ByteArray(Vec<i8>),
    String(NBTString),
    List(List),
    Compound(HashMap<NBTString, NBT>),
    IntArray(Vec<i32>),
    LongArray(Vec<i64>),
}

#[derive(Debug)]
pub struct NBT(pub(crate) NBTInner);

#[derive(Debug)]
pub enum Error {
    ListLength(usize),
    StringLength(usize),
    ListType(u8, NBT),
    ListOverflow
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::ListLength(len) => write!(f, "Couldn't construct NBT list because it has {len} items which exceeds the i32 limit"),
            Self::StringLength(len) => write!(f, "Can't construct NBT string because it has {len} bytes which exceeds the u16 limit"),
            Self::ListType(tag_id, value) => {
                write!(f, "Attempt to push {value:?} into list of type ")?;
                match *tag_id {
                    TAG_BYTE => write!(f, "Byte"),
                    TAG_SHORT => write!(f, "Short"),
                    TAG_INT => write!(f, "Int"),
                    TAG_LONG => write!(f, "Long"),
                    TAG_FLOAT => write!(f, "Float"),
                    TAG_DOUBLE => write!(f, "Double"),
                    TAG_BYTE_ARRAY => write!(f, "ByteArray"),
                    TAG_STRING => write!(f, "String"),
                    TAG_LIST => write!(f, "List"),
                    TAG_COMPOUND => write!(f, "Compound"),
                    TAG_INT_ARRAY => write!(f, "IntArray"),
                    TAG_LONG_ARRAY => write!(f, "LongArray"),
                    _ => unreachable!()
                }
            },
            Self::ListOverflow => write!(f, "Attmept to overflow list length over i32 limit")
        }
    }
}

pub type Result<T> = std::result::Result<T, Error>;

impl TryFrom<&str> for NBTString {
    type Error = Error;
    fn try_from(value: &str) -> Result<Self> {
        if value.len() > u16::MAX as usize {
            return Err(Error::StringLength(value.len()));
        }
        Ok(NBTString(value.replace('\x00', "\u{C080}")))
    }
}

impl Into<String> for NBTString {
    fn into(self) -> String {
        self.0.replace('\u{C080}', "\x00")
    }
}

macro_rules! impl_nbt_convert {
    ($p:path, $t:ty) => {
        impl From<$t> for NBT {
            fn from(value: $t) -> Self {
                NBT($p(value))
            }
        }

        impl TryInto<$t> for NBT {
            type Error = Self;

            fn try_into(self) -> std::result::Result<$t, Self> {
                match self {
                    NBT($p(value)) => Ok(value),
                    _ => Err(self),
                }
            }
        }
    };
}

macro_rules! impl_nbt_convert_vec {
    ($p:path, $t:ty) => {
        impl TryFrom<Vec<$t>> for NBT {
            type Error = Error;
            fn try_from(value: Vec<$t>) -> Result<Self> {
                if value.len() > i32::MAX as usize {
                    return Err(Error::ListLength(value.len()));
                }
                Ok(NBT($p(value)))
            }
        }

        impl TryInto<Vec<$t>> for NBT {
            type Error = Self;

            fn try_into(self) -> std::result::Result<Vec<$t>, Self> {
                match self {
                    NBT($p(value)) => Ok(value),
                    _ => Err(self),
                }
            }
        }
    };
}

impl_nbt_convert!(NBTInner::Byte, i8);
impl_nbt_convert!(NBTInner::Short, i16);
impl_nbt_convert!(NBTInner::Int, i32);
impl_nbt_convert!(NBTInner::Long, i64);
impl_nbt_convert!(NBTInner::Float, f32);
impl_nbt_convert!(NBTInner::Double, f64);
impl_nbt_convert!(NBTInner::String, NBTString);
impl_nbt_convert!(NBTInner::List, List);
impl_nbt_convert!(NBTInner::Compound, HashMap<NBTString, NBT>);

impl_nbt_convert_vec!(NBTInner::ByteArray, i8);
impl_nbt_convert_vec!(NBTInner::IntArray, i32);
impl_nbt_convert_vec!(NBTInner::LongArray, i64);

impl TryFrom<String> for NBT {
    type Error = Error;
    fn try_from(value: String) -> Result<Self> {
        Ok(Self(NBTInner::String(value.as_str().try_into()?)))
    }
}

impl TryInto<String> for NBT {
    type Error = Self;
    fn try_into(self) -> std::result::Result<String, Self> {
        match self {
            Self(NBTInner::String(s)) => Ok(s.into()),
            _ => Err(self)
        }
    }
}

impl fmt::Display for NBTString {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0.replace('\u{C080}', "\x00"))
    }
}

impl List {
    pub fn new() -> Self {
        Self(TAG_END, Vec::new())
    }

    pub fn with_capacity(capacity: usize) -> Self {
        Self(TAG_END, Vec::with_capacity(capacity))
    }

    pub fn tag(&self) -> u8 {
        self.0
    }

    pub fn vec(&self) -> &Vec<NBT> {
        &self.1
    }

    pub fn parts(&self) -> (u8, &Vec<NBT>) {
        (self.0, &self.1)
    }

    pub fn push(&mut self, value: NBT) -> Result<()> {
        if self.1.len() != 0 {
            if value.tag() != self.0 {
                return Err(Error::ListType(self.0, value));
            }
        } else {
            self.0 = value.tag()
        }
        self.1.push(value);
        Ok(())
    }

    pub fn pop(&mut self) -> Result<Option<NBT>> {
        if self.1.len() == u16::MAX as usize {
            return Err(Error::ListOverflow);
        }
        Ok(self.1.pop())
    }
}

impl fmt::Debug for NBTInner {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Byte(_) => write!(f, "Byte"),
            Self::Short(_) => write!(f, "Short"),
            Self::Int(_) => write!(f, "Int"),
            Self::Long(_) => write!(f, "Long"),
            Self::Float(_) => write!(f, "Float"),
            Self::Double(_) => write!(f, "Double"),
            Self::ByteArray(_) => write!(f, "ByteArray"),
            Self::String(_) => write!(f, "String"),
            Self::List(_) => write!(f, "List"),
            Self::Compound(_) => write!(f, "Compound"),
            Self::IntArray(_) => write!(f, "IntArray"),
            Self::LongArray(_) => write!(f, "LongArray")
        }
    }
}

impl NBT {
    pub fn deserialise(reader: &mut dyn Read) -> serial::Result<Self> {
        Ok(read_compound_payload(reader, 0)?.into())
    }

    pub fn serialise(&self, writer: &mut dyn Write) -> serial::Result<()> {
        if let Self(NBTInner::Compound(map)) = self {
            write_compound_payload(writer, map, 0)
        } else {
            Err(serial::Error::NotCompound)
        }
    }

    pub fn tag(&self) -> u8 {
        match self {
            Self(NBTInner::Byte(_)) => TAG_BYTE,
            Self(NBTInner::Short(_)) => TAG_SHORT,
            Self(NBTInner::Int(_)) => TAG_INT,
            Self(NBTInner::Long(_)) => TAG_LONG,
            Self(NBTInner::Float(_)) => TAG_FLOAT,
            Self(NBTInner::Double(_)) => TAG_DOUBLE,
            Self(NBTInner::ByteArray(_)) => TAG_BYTE_ARRAY,
            Self(NBTInner::String(_)) => TAG_STRING,
            Self(NBTInner::List(_)) => TAG_LIST,
            Self(NBTInner::Compound(_)) => TAG_COMPOUND,
            Self(NBTInner::IntArray(_)) => TAG_INT_ARRAY,
            Self(NBTInner::LongArray(_)) => TAG_LONG_ARRAY,
        }
    }

    fn fmt(
        &self,
        indentation: &mut String,
        top: bool,
        comma: bool,
        f: &mut fmt::Formatter<'_>,
    ) -> fmt::Result {
        match self {
            Self(NBTInner::Byte(byte)) => write!(f, "{byte}b")?,
            Self(NBTInner::Short(short)) => write!(f, "{short}s")?,
            Self(NBTInner::Int(int)) => write!(f, "{int}i")?,
            Self(NBTInner::Long(long)) => write!(f, "{long}L")?,
            Self(NBTInner::Float(float)) => write!(f, "{float}f")?,
            Self(NBTInner::Double(double)) => write!(f, "{double}d")?,
            Self(NBTInner::ByteArray(bytes)) => {
                writeln!(f, "[ B;")?;
                indentation.push_str("  ");
                let mut iter = bytes.iter();
                while let Some(byte) = iter.next() {
                    write!(f, "{indentation}{byte}b")?;
                    if iter.len() != 0 {
                        write!(f, ",")?;
                    }
                    writeln!(f)?;
                }
                indentation.pop();
                indentation.pop();
                write!(f, "{indentation}]")?;
            }
            Self(NBTInner::String(string)) => write!(f, "\"{string}\"")?,
            Self(NBTInner::List(List(_, vec))) => {
                writeln!(f, "[")?;
                indentation.push_str("  ");
                let mut iter = vec.iter();
                while let Some(item) = iter.next() {
                    write!(f, "{indentation}")?;
                    Self::fmt(item, indentation, false, iter.len() != 0, f)?;
                }
                indentation.pop();
                indentation.pop();
                write!(f, "{indentation}]")?;
            }
            Self(NBTInner::Compound(map)) => {
                writeln!(f, "{{")?;
                indentation.push_str("  ");
                let mut iter = map.iter();
                while let Some((name, item)) = iter.next() {
                    write!(f, "{indentation}\"{name}\": ")?;
                    Self::fmt(item, indentation, false, iter.len() != 0, f)?;
                }
                indentation.pop();
                indentation.pop();
                write!(f, "{indentation}}}")?;
            }
            Self(NBTInner::IntArray(ints)) => {
                writeln!(f, "[ I;")?;
                indentation.push_str("  ");
                let mut iter = ints.iter();
                while let Some(int) = iter.next() {
                    write!(f, "{indentation}{int}i")?;
                    if iter.len() != 0 {
                        write!(f, ",")?;
                    }
                    writeln!(f)?;
                }
                indentation.pop();
                indentation.pop();
                write!(f, "{indentation}]")?;
            }
            Self(NBTInner::LongArray(longs)) => {
                writeln!(f, "[ L;")?;
                indentation.push_str("  ");
                let mut iter = longs.iter();
                while let Some(long) = iter.next() {
                    write!(f, "{indentation}{long}l")?;
                    if iter.len() != 0 {
                        write!(f, ",")?;
                    }
                    writeln!(f)?;
                }
                indentation.pop();
                indentation.pop();
                write!(f, "{indentation}]")?;
            }
        }
        if !top {
            if comma {
                write!(f, ",")?;
            }
            writeln!(f)?;
        }
        Ok(())
    }
}

impl fmt::Display for NBT {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        Self::fmt(self, &mut String::new(), true, false, f)
    }
}
